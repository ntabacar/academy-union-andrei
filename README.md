### README

---
---

# What is this?

This is a simple exercise to learn the basics of *npm packages* and *git*.

# Why?

It is part of a **coding Academy** I am attending.

# Ok, but why all this fuss in the README?

I'm just playing around with markdown, lol.

# Hmmm, what does it do then?

The exercise consisted in each student creating an npm package with an `index.js` printing 2 characters.
The next step was to put those packages into a single one and print the final string `We are the best!!!` by combining all the modules.

---

# Code

All the modules have pretty much this snippet in their `index.js`:

```js
exports.myFunction = function () {
	// eslint-disable-next-line no-undef-init
	return "myString";
}
```

To print the full string i imported my colleagues' modules first (with `require(package-name)`) and then used this nice recursive function:

```js
function printFullString(arr, i, s = "") {
	if (i < arr.length) {
		s += arr[i];
		return printFullString(arr, i+1, s);
	};
	return console.log(s);
};
```

# How do I install this on my machine?

Well, it's actually really simple!

You have two ways:

* Clone this git directory by downloading its zip or through your bash console:
	* Via SSH (`git clone git@gitlab.com:ntabacar/academy-union-andrei.git`);
	* Via HTTPS (`git clone https://gitlab.com/ntabacar/academy-union-andrei.git`).
		* Go into that directory and write `npm install`

* If you have npm (you should anyway if you want to build this project):
	* Go into the directory you created for this scope;
	* In your bash write `npm i academy-union-andrei` and it will take care of everything for you.

* Now all you have to do to run the script that will print the final string is `node start`

# Wow! Is this over yet?

Nope, still have to try tables in markdown hehe.

Index | Joke
------|------
0 | Never trust math teachers who use graph paper. They're always plotting something.
1 | Here’s some advice: At a job  interview, tell them you’re willing to give 110 percent. Unless the job is a statistician. *Comedian Adam Gropman*
2 | I put my root beer in a square glass. Now it's just beer.


Special thanks to our tutor for today, mr. *Arnel Helja*.