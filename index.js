const b = require('esercizio-javascript');
const c = require('printmeplease');
const d = require('academy-union-gc');
const e = require('academy-pc');
const f = require('academy-union_mdod');
const g = require('academy_test');
const h = require('academy-union_project');
const i = require('academy-unionpt');

let printMyString = function () {
	// eslint-disable-next-line no-undef-init
	return "es";
}

//This function is recursive
function printFullString(arr, i, s = "") {
	if (i < arr.length) {
		s += arr[i];
		return printFullString(arr, i+1, s);
	};
	return console.log(s);
};

let lst = [h.displayText(), d.lettere(), c.updateString(), b.printLorMin(),
	       f.nomeCriptico(), g.myLetters(), printMyString(), e.pcString(), i.updateString()];

printFullString(lst, 0);

exports.printMyString = printMyString;
exports.printFullString = printFullString;